--- Echo policy
-- Print the request back to the client and optionally set a status code.
-- Also can interrupt the execution and skip the current phase or
-- the whole processing of the request.
-- @Author: Filippo ceresoli
-- @Version: 0.1 - created
-- @Version: 0.2 - fixed some return code and error handling
--    - If the policy is not configured will skip
--    - If a header is missing (Body: Authentication parameters missing, status:403, headers: text/plain; charset=us-ascii)
--    - Authentication error (Body: Authentication error, status:403, headers: text/plain; charset=us-ascii)
-- @Version: 0.3 - added key and user caching


local _M  = require('apicast.policy').new('AutoRegisterPolicy','0.3')
local cjson = require('cjson')
local lrucache  = require('resty.lrucache')


local OK = 0;
local ERROR = 1;

local new = _M.new

local INFO = ngx.WARN;
local WARN = ngx.WARN;
local ERROR = ngx.ERROR;

function _M.new(configuration)
  local policy = new(configuration)
  ngx.log(INFO, '- AutoRegisterPolicy : New instance...')
  if configuration then
    policy.lrucache = nil;
    policy.key_cache = nil;
    policy.user_cache = nil;
    policy.isConfigured = true;
    ngx.log(INFO, '- AutoRegisterPolicy : Loading Configuration...')

    policy.configurationPlanName = configuration.configurationPlanName -- plan Name for application registration
    policy.APIName = configuration.APIName -- API Name for application registration

    policy.apiServiceToken = configuration.apiServiceToken -- service token for API access
    policy.apiAccessToken = configuration.apiAccessToken -- administration token for API access


    policy.applicationName = configuration.applicationName -- application name for registration;
    policy.threeScaleBaseURL = configuration.threeScaleBaseURL -- 3Scale base URL for API call
    policy.userHeaderName = configuration.userHeaderName -- HTTP header name for extracting username
    policy.appTokenHeaderName = configuration.appTokenHeaderName -- HTTP header name for extracting app_key
    policy.emailHeaderName = configuration.emailHeaderName -- HTTP header name for extracting email to be used for registration
    policy.orgHeaderName = configuration.orgHeaderName -- HTTP header name for extracting user organization to be used for registration (TPP)
    policy.registrationPassword = configuration.registrationPassword -- User registration Default Password

    -- caching configuration  since Ver 0.3
    policy.cachingEnabled = configuration.cachingEnabled -- true if caching is enabled
    policy.cacheSize = configuration.cacheSize
    policy.cacheTTL = configuration.cacheTTL



    ngx.log(INFO, '- AutoRegisterPolicy : Config: configurationPlanName:',policy.configurationPlanName);
    ngx.log(INFO, '- AutoRegisterPolicy : Config: APIName:',policy.APIName);
    ngx.log(INFO, '- AutoRegisterPolicy : Config: apiServiceToken:',policy.apiServiceToken);
    ngx.log(INFO, '- AutoRegisterPolicy : Config: apiAccessToken:',policy.apiAccessToken);
    ngx.log(INFO, '- AutoRegisterPolicy : Config: applicationName:',policy.applicationName);
    ngx.log(INFO, '- AutoRegisterPolicy : Config: threeScaleBaseURL:',policy.threeScaleBaseURL);
    ngx.log(INFO, '- AutoRegisterPolicy : Config: userHeaderName:',policy.userHeaderName);
    ngx.log(INFO, '- AutoRegisterPolicy : Config: appTokenHeaderName:',policy.appTokenHeaderName);
    ngx.log(INFO, '- AutoRegisterPolicy : Config: emailHeaderName:',policy.emailHeaderName);
    ngx.log(INFO, '- AutoRegisterPolicy : Config: orgHeaderName:',policy.orgHeaderName);
    ngx.log(INFO, '- AutoRegisterPolicy : Config: registrationPassword:',policy.registrationPassword);
    ngx.log(INFO, '- AutoRegisterPolicy : Config: cachingEnabled:',policy.cachingEnabled);
    ngx.log(INFO, '- AutoRegisterPolicy : Config: cacheSize:',policy.cacheSize);
    ngx.log(INFO, '- AutoRegisterPolicy : Config: cacheTTL:',policy.cacheTTL);
    ngx.log(INFO, '- AutoRegisterPolicy : Config: END');
  else
    policy.isConfigured = false;
    ngx.log(WARN, '- AutoRegisterPolicy : Config: configuration missing, the policy will be disabled until configured');
  end
  return policy
end



function _M:rewrite()

end

-- o meglio la inseriamo nell'auth
-- function _M:verifyCaching()
--   if self.cachingEnabled == true then
--     self.lrucache  = require('resty.lrucache')
--     if not self.user_cache then
--       ngx.log(INFO,'- AutoRegisterPolicy : caching: creating new user_cache... ')
--       self.user_cache = self.lrucache.new(self.cacheSize);
--       self.user_cache:set("cache_size",self.cacheSize,nil)
--     elseif(self.user_cache:get("cache_size") ~= self.cacheSize) then
--       ngx.log(INFO,'- AutoRegisterPolicy : caching: configuration changed! recreating user_cache... ')
--       self.user_cache.flush_all();
--      self.user_cache = lrucache.new(self.cacheSize);
--      self.user_cache:set("cache_size",self.cacheSize,nil)
--     end
--     if not self.key_cache then
--       ngx.log(INFO,'- AutoRegisterPolicy : caching: creating new self.key_cache... ')
--       self.key_cache = self.lrucache.new(self.cacheSize);
--       self.key_cache:set("cache_size",self.cacheSize,nil)
--     elseif(self.key_cache:get("cache_size") ~= self.cacheSize) then
--       ngx.log(INFO,'- AutoRegisterPolicy : caching: configuration changed! recreating self.key_cache... ')
--       self.key_cache.flush_all();
--       self.key_cache = lrucache.new(self.cacheSize);
--       self.key_cache:set("cache_size",self.cacheSize,nil)
--     end
--   else
--    self.user_cache = nil;
--     self.key_cache = nil;
--   end
-- end
-- replace lrucache with ngx.shared.registry
function _M:verifyCaching()
  if self.cachingEnabled == true then
    if not self.user_cache then
      ngx.log(INFO,'- AutoRegisterPolicy : caching: creating new user_cache... ')
      self.user_cache = lrucache.new(self.cacheSize);
      self.user_cache:set("cache_size",self.cacheSize,nil)
    elseif(self.user_cache:get("cache_size") ~= self.cacheSize) then
      ngx.log(INFO,'- AutoRegisterPolicy : caching: configuration changed! recreating user_cache... ')
      self.user_cache.flush_all();
     self.user_cache = lrucache.new(self.cacheSize);
     self.user_cache:set("cache_size",self.cacheSize,nil)
    end
    if not self.key_cache then
      ngx.log(INFO,'- AutoRegisterPolicy : caching: creating new self.key_cache... ')
      self.key_cache = lrucache.new(self.cacheSize);
      self.key_cache:set("cache_size",self.cacheSize,nil)
    elseif(self.key_cache:get("cache_size") ~= self.cacheSize) then
      ngx.log(INFO,'- AutoRegisterPolicy : caching: configuration changed! recreating self.key_cache... ')
      self.key_cache.flush_all();
      self.key_cache = lrucache.new(self.cacheSize);
      self.key_cache:set("cache_size",self.cacheSize,nil)
    end
  else
   self.user_cache = nil;
    self.key_cache = nil;
  end
end

function _M:access()
  if (self.isConfigured) then

    self:verifyCaching()
    ngx.log(INFO, '- AutoRegisterPolicy : access: start');
    local username, email, org, appToken, callResult, errorDesc,user_id,ret_status,http_error;
    local user_id;
    ngx.log(INFO, '- AutoRegisterPolicy : access: extracting HTTP Headers');
    username, email, org, appToken, ret_status = self:extractHTTPHeaders();
    ngx.log(INFO, '- AutoRegisterPolicy : access: extracted HTTP Header - username',username);
    ngx.log(INFO, '- AutoRegisterPolicy : access: extracted HTTP Header - email',email);
    ngx.log(INFO, '- AutoRegisterPolicy : access: extracted HTTP Header - org',org);
    ngx.log(INFO, '- AutoRegisterPolicy : access: extracted HTTP Header - appToken',appToken);

    if ret_status == ERROR then
      self.deny_request('Authentication parameters missing');
    end

    ret_status, user_id, http_error = self:checkUserExistence(username,email,org);

    if ret_status == ERROR then
      self.deny_request('Authentication Error');
    end
    ret_status = self:checkAPIKeyExistence(appToken,user_id);
    if ret_status == ERROR then
      self.deny_request('Authentication Error');
    end
  else
    ngx.log(WARN, '- AutoRegisterPolicy : access: policy not configured, skipping ghost authentication');
  end
end

function _M.deny_request(error_msg)
  ngx.status = ngx.HTTP_FORBIDDEN
  ngx.say(error_msg)
  ngx.exit(ngx.status)
end
-- call Account Find API
-- curl -v  -X GET "https://3scale-admin.192.168.99.103.nip.io/admin/api/accounts/find.xml?access_token=3c6bdab125c481b63a98d93bd421aab331acaded36fd5e71840fe86f1f4b0b85&username=ccccc"
-- Sample response (if exists)
-- {
--   "account": {
--     "id": 5,
--     "created_at": "2019-02-28T10:42:21Z",
--     "updated_at": "2019-02-28T10:42:21Z",
--     "credit_card_stored": false,
--     "monthly_billing_enabled": true,
--     "monthly_charging_enabled": true,
--     "state": "approved",
--     "links": [
--         {
--             "rel": "self",
--             "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/accounts/5"
--         },
--         {
--             "rel": "users",
--             "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/accounts/5/users"
--         }
--     ],
--     "org_name": "testOrg2"
-- }
-- }
-- Function for verify user existence:
-- input values:
--  - username: the username for the lookup
--  - email: the administration email (used for registration)
--  - org: the organization name (used for registration)
-- outputs:
--  - Status: one of
--      - OK: the user is already present
--      - KO: an error occourred while processing the request
--  - user_id: the registered user ID, to be used for further calls (nil if an error occurs)
--  - http_status: the http status code of the last call
--  - http_error: the http_error from the last call
function _M:checkUserExistence(username,email,org)
  ngx.log(INFO, '- AutoRegisterPolicy : checkUserExistence: start');
  local user_id
  local apiPath;
  local args;
  local http_status_code, http_error, json_body;
  local tabled_body = {} ;
  if self.user_cache then
    user_id = self.user_cache:get(username .. '@' .. org);
    ngx.log(INFO,"- AutoRegisterPolicy : caching: entry found in user_cache for " .. username .. '@' .. org .. ': ',user_id )
  end
  if not user_id then
    apiPath="/admin/api/accounts/find.json";
    args="&username=" .. username;


    local tabled_body = {} ;
    ngx.log(INFO, '- AutoRegisterPolicy : checkUserExistence: invokeAPI');
    http_status_code, http_error, json_body = self:invokeAPI(apiPath,args,"GET",self.apiAccessToken);
    ngx.log(INFO, '- AutoRegisterPolicy : checkUserExistence: api invocation return: ',http_status_code);
    if http_status_code == 404 then
      ngx.log(INFO, '- AutoRegisterPolicy : checkUserExistence: registering user... ');
      ret_status, user_id, http_error = self:registerUser(username,email,org);
      ngx.log(INFO, '- AutoRegisterPolicy : checkUserExistence: user registered with ID: ',user_id);
    elseif http_status_code ~= 200 then
      return ERROR, nil, http_error;
    else
      tabled_body = cjson.decode(json_body);
      user_id = tabled_body["account"]["id"];
      ngx.log(INFO, '- AutoRegisterPolicy : checkUserExistence: user registered with ID: ',user_id);

    end
  end
  if self.user_cache then
    ngx.log(INFO,"- AutoRegisterPolicy : caching: registering entry in user_cache for " .. username .. '@' .. org .. ': ',user_id )
   self.user_cache:set(username .. '@' .. org,user_id,self.cacheTTL);
  end
  return OK, user_id, nil;
end
-- call Application Find API
-- curl -v  -X GET "https://3scale-admin.192.168.99.102.nip.io/admin/api/applications/find.json?access_token=36e4a53c6791268ac08fda03fbb612567a6a32579f10997155d65ecaa5ac3560&user_key=36e4a53c6791268ac08fda03fbb612567a6a32579f10997155d65ecaa5ac3560"
--
-- Sample response:
-- {
--  "application": {
--    "id": 11,
--    "state": "live",
--    "enabled": true,
--    "end_user_required": false,
--    "created_at": "2019-02-28T10:42:21Z",
--    "updated_at": "2019-02-28T10:42:21Z",
--    "service_id": 2,
--    "plan_id": 9,
--    "account_id": 5,
--    "first_traffic_at": null,
--    "first_daily_traffic_at": null,
--    "user_key": "022621ddd5e3b8a93c065707612f483e",
--    "provider_verification_key": "b492a80288b551e60aa48bcfa52ab3dd",
--    "links": [
--        {
--            "rel": "self",
--            "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/accounts/5/applications/11"
--        },
--        {
--            "rel": "service",
--            "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/services/2"
--        },
--        {
--            "rel": "account",
--            "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/accounts/5"
--        },
--        {
--            "rel": "plan",
--            "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/services/2/application_plans/9"
--        },
--        {
--            "rel": "keys",
--            "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/accounts/5/applications/11/keys"
--        },
--        {
--            "rel": "referrer_filters",
--            "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/accounts/5/applications/11/referrer_filters"
--        }
--    ],
--    "name": "API signup",
--    "description": "API signup"
--}
--}
function _M:checkAPIKeyExistence(user_key,user_id)
  local key;
  if self.key_cache then
    key = self.key_cache:get(user_id .. '@' .. user_key);
    ngx.log(INFO,"- AutoRegisterPolicy : caching: entry found in self.key_cache for " .. user_id .. '@' .. user_key .. ': ',key )
  end
  if not key then
    local apiPath="/admin/api/applications/find.json"
    local args="&user_key=" .. user_key
    local http_status_code, http_error, json_body;
    ngx.log(INFO, '- AutoRegisterPolicy : checkAPIKeyExistence: start');
    http_status_code, http_error, json_body = self:invokeAPI(apiPath,args,"GET",self.apiAccessToken);
    if http_status_code == 404 then
      ngx.log(INFO, '- AutoRegisterPolicy : checkAPIKeyExistence: registerAPIKey');
      http_status_code = self:registerAPIKey(user_key,user_id);
      if http_status_code ~= 201 and http_status_code ~= 200  then
        ngx.log(INFO, '- AutoRegisterPolicy : checkAPIKeyExistence:registerAPIKey KO: ',http_status_code);
        return ERROR;
      end
    elseif http_status_code == 200 then
      ngx.log(INFO, '- AutoRegisterPolicy : checkAPIKeyExistence:registerAPIKey OK');
    else
      ngx.log(INFO, '- AutoRegisterPolicy : checkAPIKeyExistence:registerAPIKey KO: ',http_status_code);
      return ERROR;
    end
  end
  if self.key_cache then
    ngx.log(INFO,"- AutoRegisterPolicy : caching: registering entry in self.key_cache for " .. user_id .. '@' .. user_key .. ': ',user_key )
    self.key_cache:set(user_id .. '@' .. user_key,user_key,self.cacheTTL);
  end
  return OK;

end
-- call Signup Express (Account Create)
-- curl -v  -X POST "https://3scale-admin.192.168.99.103.nip.io/admin/api/signup.json" -d 'access_token=3c6bdab125c481b63a98d93bd421aab331acaded36fd5e71840fe86f1f4b0b85&org_name=testOrg&username=username&email=username%40testOrg.com&password=password&application_plan_id=9'
-- sample request:
-- {
--	"access_token" : "3c6bdab125c481b63a98d93bd421aab331acaded36fd5e71840fe86f1f4b0b85",
--	"org_name" : "testOrg2",
--	"username" : "username2",
--	"email" : "username2@testOrg2.com",
--	"password" : "password",
--	"application_plan_id" : "9"
-- }
--

-- sample response:
-- {
--  "account": {
--    "id": 5,
--    "created_at": "2019-02-28T10:42:21Z",
--    "updated_at": "2019-02-28T10:42:21Z",
--    "credit_card_stored": false,
--    "monthly_billing_enabled": true,
--    "monthly_charging_enabled": true,
--    "state": "approved",
--    "links": [
--        {
--            "rel": "self",
--            "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/accounts/5"
--        },
--        {
--            "rel": "users",
--            "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/accounts/5/users"
--        }
--    ],
--    "org_name": "testOrg2"
--}
--}
-- call Account Approve ??
-- call self registerAPIKey
function _M:registerUser(username,email,org)

  ngx.log(INFO, '- AutoRegisterPolicy : registerUser: start');
  local apiPath="/admin/api/signup.json"
  local args = {};
  local http_status_code, http_error, json_body,user_id;
  args["org_name"]=org;
  args["username"]=username;
  args["email"]=email;
  args["password"]=self.registrationPassword;
  ngx.log(INFO, '- AutoRegisterPolicy : Retrieving Plan ID...')

  http_status_code, http_error, json_body = self:invokeAPI(apiPath,args,"POST",self.apiAccessToken);
  tabled_body = cjson.decode(json_body);

  if http_status_code == 201 then
    user_id = tabled_body["account"]["id"];
    ngx.log(INFO, '- AutoRegisterPolicy : registerUser: user created with ID: ',user_id);
    if user_cache then
     self.user_cache:set(username .. '@' .. org,user_id,self.cacheTTL);
    end
  else
    ngx.log(INFO, '- AutoRegisterPolicy : registerUser: Failed! ',http_status_code);
    return ERROR, nil, http_error
  end
  return OK, user_id, nil


end
-- call Application Create
-- curl -v  -X POST "https://3scale-admin.192.168.99.103.nip.io/admin/api/accounts/<USER_ID>/applications.json" -d 'access_token=s&plan_id=s&name=s&description=s&user_key=s'
-- Sample Request:
-- {
--	"access_token" : "3c6bdab125c481b63a98d93bd421aab331acaded36fd5e71840fe86f1f4b0b85",
--	"plan_id" : "9",
--	"name" : "app_username2",
--	"description" : "this application has created dynamically by the AutoRegisterPolicy",
--	"user_key" : "000111222333444555aaabbbccc"
-- }
--
-- Sample Response:
--{
--  "application": {
--      "id": 12,
--      "state": "live",
--      "enabled": true,
--      "end_user_required": false,
--      "created_at": "2019-02-28T11:46:23Z",
--      "updated_at": "2019-02-28T11:46:23Z",
--      "service_id": 2,
--      "plan_id": 9,
--      "account_id": 5,
--      "first_traffic_at": null,
--      "first_daily_traffic_at": null,
--      "user_key": "000111222333444555aaabbbccc",
--      "provider_verification_key": "01b714900883cfa77f3279edcdf9d54b",
--      "links": [
--          {
--              "rel": "self",
--              "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/accounts/5/applications/12"
--          },
--          {
--              "rel": "service",
--              "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/services/2"
--          },
--          {
--              "rel": "account",
--              "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/accounts/5"
--          },
--          {
--              "rel": "plan",
--              "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/services/2/application_plans/9"
--          },
--          {
--              "rel": "keys",
--              "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/accounts/5/applications/12/keys"
--          },
--          {
--              "rel": "referrer_filters",
--              "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/accounts/5/applications/12/referrer_filters"
--          }
--      ],
--      "name": "app_username2",
--      "description": "this application has created dynamically by the AutoRegisterPolicy"
--  }
--}


function _M:registerAPIKey(appToken,user_id)
  ngx.log(INFO, '- AutoRegisterPolicy : registerAPIKey: start');
  local apiPath="/admin/api/accounts/" .. user_id .. "/applications.json"
  local args = {};
  local http_status_code, http_error, json_body,app_id;
  local autoRegisterPlanID = self:retrieveConfigurationPlanID(self.APIName,self.configurationPlanName);

  args["plan_id"]=autoRegisterPlanID;
  args["name"]=self.applicationName;
  args["description"]= "this application has created dynamically by the AutoRegisterPolicy";
  args["user_key"]=appToken;
  ngx.log(INFO, '- AutoRegisterPolicy : registerAPIKey: invoking API...');
  http_status_code, http_error, json_body = self:invokeAPI(apiPath,args,"POST",self.apiAccessToken);
  tabled_body = cjson.decode(json_body);

  if http_status_code == 201 then
    app_id = tabled_body["application"]["id"];
    ngx.log(INFO, '- AutoRegisterPolicy : registerAPIKey: success, new app_id: ',app_id);
    return app_id, OK,nil,nil
  else
    ngx.log(INFO, '- AutoRegisterPolicy : registerAPIKey: FAIL! ',http_status_code);
  end

end

function _M:invokeAPI(apiPath,args,httpMethod,accessToken)
  local http = require ('resty.http')
  local httpc = http.new()
  ngx.log(INFO, '- AutoRegisterPolicy : invokeAPI: start');
  local http_status_code;
  local http_error;
  local json_body;
  local fullApiURL;
  local params ={};
  local headers = {}
  if httpMethod == "GET" then
    if args then

      fullApiURL=self.threeScaleBaseURL .. "/" .. apiPath .. "?access_token=".. accessToken  .. args .. "";
    else
      fullApiURL=self.threeScaleBaseURL .. "/" .. apiPath .. "?access_token=".. accessToken  .. "";
    end
  elseif httpMethod == "POST" then
      fullApiURL=self.threeScaleBaseURL .. "/" .. apiPath;
      args["access_token"]=accessToken;
      params["body"] = cjson.encode(args);
  end
  headers["Content-Type"]="application/json";
  params["method"] = httpMethod;
  params["headers"] = headers;
  params["ssl_verify"] = false;
  ngx.log(INFO, '- AutoRegisterPolicy : invokeAPI: invoking API at url',fullApiURL);
  ngx.log(INFO, '- AutoRegisterPolicy : invokeAPI: invoking API with',httpMethod);
  local res, err = httpc:request_uri(fullApiURL, params)
  if err then
    ngx.log(ngx.WARN,err)
  end
  http_status_code = res.status;
  http_error = err or res.reason;
  json_body = res.body;

   -- Use res.body to access the response
   -- When the request is successful, res will contain the following fields:
   -- status The status code.
   -- reason The status reason phrase.
   -- headers A table of headers. Multiple headers with the same field name will be presented as a table of values.
   -- body The response body
  ngx.log(INFO, '- AutoRegisterPolicy : invokeAPI: API invoked: http_status_code',http_status_code);
  ngx.log(INFO, '- AutoRegisterPolicy : invokeAPI: API invoked: http_error',http_error);
  ngx.log(INFO, '- AutoRegisterPolicy : invokeAPI: API invoked: json_body',json_body);
  return http_status_code, http_error, json_body;
end

function _M:extractHTTPHeaders()
    local username, email, appToken, org;

    username = ngx.req.get_headers()[self.userHeaderName];
    email =  ngx.req.get_headers()[self.emailHeaderName];
    appToken = ngx.req.get_headers()[self.appTokenHeaderName];
    org = ngx.req.get_headers()[self.orgHeaderName];
    if not username or not email or not appToken or not org then
      return username, email, org, appToken, ERROR;
    end;
    return username, email, org, appToken, OK;
end

-- in order to retrieve the configuration Plan:
-- retrieve the API ID from its name using the Service List API
-- curl -v  -X GET "https://3scale-admin.192.168.99.103.nip.io/admin/api/services.json?access_token=3c6bdab125c481b63a98d93bd421aab331acaded36fd5e71840fe86f1f4b0b85"
-- Sample Response:
--{
--  "services": [
--    {
--        "service": {
--            "id": 2,
--            "name": "Echo API",
--            "state": "incomplete",
--            "system_name": "api",
--            "end_user_registration_required": true,
--            "backend_version": "1",
--            "deployment_option": "hosted",
--            "support_email": "admin@3scale.192.168.99.103.nip.io",
--            "created_at": "2019-02-28T09:26:19Z",
--            "updated_at": "2019-02-28T13:08:29Z",
--            "links": [
--                {
--                    "rel": "metrics",
--                    "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/services/2/metrics"
--                },
--                {
--                    "rel": "end_user_plans",
--                    "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/services/2/end_user_plans"
--                },
--                {
--                    "rel": "self",
--                    "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/services/2"
--                },
--               {
--                    "rel": "service_plans",
--                    "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/services/2/service_plans"
--                },
--                {
--                    "rel": "application_plans",
--                    "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/services/2/application_plans"
--                },
--                {
--                    "rel": "features",
--                    "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/services/2/features"
--                }
--            ]
--        }
--    }, ...
--  }
-- retrieve the configuration plan ID from API ID + Configuration plan name
-- curl -v  -X GET "https://3scale-admin.192.168.99.103.nip.io/admin/api/services/2/application_plans.json?access_token=3c6bdab125c481b63a98d93bd421aab331acaded36fd5e71840fe86f1f4b0b85"
-- Sample Response:
--
--{
--  "plans": [
--    {
--        "application_plan": {
--            "id": 7,
--            "name": "Basic",
--            "state": "published",
--            "setup_fee": 0,
--            "cost_per_month": 0,
--            "trial_period_days": 0,
--            "cancellation_period": 0,
--            "approval_required": false,
--            "default": true,
--            "created_at": "2019-02-28T09:28:51Z",
--            "updated_at": "2019-02-28T09:28:52Z",
--            "custom": false,
--            "system_name": "basic",
--            "end_user_required": false,
--            "links": [
--                {
--                    "rel": "service",
--                    "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/services/2"
--                },
--                {
--                    "rel": "self",
--                    "href": "https://3scale-admin.192.168.99.103.nip.io/admin/api/services/2/application_plans/7"
--                }
--            ]
--        }
--    }, ...
--  }
function _M:retrieveConfigurationPlanID(APIName,configurationPlanName)
  local API_id;
  local configurationPlan_id;
  local apiPath="/admin/api/services.json"
  local args = nil;
  local http_status_code, http_error, json_body,app_id;
  http_status_code, http_error, json_body = self:invokeAPI(apiPath,args,"GET",self.apiAccessToken);
  if http_status_code ~= 200 then
    return nil, ERROR;
  end
  tabled_body = cjson.decode(json_body);
  local services = {};
  services=tabled_body.services;
  local i

  for i=1,table.getn(services) do
    if services[i].service.name == APIName then
      API_id = services[i].service.id;
      break;
    end
  end

  -- retrieve plans for the service
  apiPath="/admin/api/services/" .. API_id .. "/application_plans.json"
  args = nil;
  http_status_code, http_error, json_body = self:invokeAPI(apiPath,args,"GET",self.apiAccessToken);
  if http_status_code ~= 200 then
    return nil, ERROR;
  end
  tabled_body = cjson.decode(json_body);
  local services = {};
  plans=tabled_body.plans;
  local i

  for i=1,table.getn(plans) do
    if plans[i].application_plan.name == configurationPlanName then
      return plans[i].application_plan.id, OK;
    end
  end
end


return _M
